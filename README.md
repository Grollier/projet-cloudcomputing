# Projet architecture AMQP - Cloud Computing

## Outils nécéssaires

* Docker
* Docker-compose
* Node

## Exécution de l'application

Après avoir cloné et s'être placé à la racine du repository, vous pouvez lancer la commande `docker-compose up` qui permet dans lancer les containers mysql et rabbitmq. 

Pour accéder à l'interface rabbitmq, il s'agit du port 15672 sur localhost.

Lors de chaque démarrage, un script SQL est lancé pour se placer sur la bonne base de données et pour créer la table associée au projet (Commande). Pour clean la BD à chaque utilisation la commande `docker-compose down -v` permet de supprimer les volumes existant sur le projet. 

Pour lancer l'API, il suffit de se placer à la racine également et de lancer la commande `node index.js`. Les routes disponibles sont : 
* /createCommand pour créer une commande
* /getCommand pour afficher l'état des commandes

N'étant pas un expert en NodeJS, les APIs et la réalisation globale du projet ont été faites de la manière la plus basique possible. 

N'hésitez pas à me contacter via Teams (Théo GROLLIER) si vous avez des questions.

