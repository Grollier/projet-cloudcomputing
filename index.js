const express = require('express')
const amqp = require('amqplib')
const mysql = require('mysql')
const app = express()

// Connexion à la base de données.
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: 'projet'
})

db.connect(function(err) {
  if (err) {
    throw err;
  } else {
    console.log("Connecté à la BDD Mysql")
  }
})

const flag = "Commande en cours de traitement"
const flag_traite = "commande traitée"
const creer_plat = "créer le plat"

// Fonctions concernant le worker.
let channel, connection;  
async function connectQueueAndSendDatas() {   
    try {
      connection = await amqp.connect("amqp://localhost:5672");
      channel = await connection.createChannel()
        
      await channel.assertQueue("command-queue", {durable: true});
      console.log('Bien connecté à la queue')
    
      sendMessageExchange(creer_plat)
    
      sendData(flag_traite)
        
    } catch (error) {
        console.log(error)
    }
}

async function sendData(data) {  
  await channel.sendToQueue("command-queue", Buffer.from(JSON.stringify(data)));

  await channel.consume("command-queue", () => {
    db.query('UPDATE Commande SET flag = ? WHERE flag = ?', [data,flag], (error, results) => {
      if (error) throw error;
      console.log(results.affectedRows + ' row inserted');
    })
  }, {noAck: true})

  await channel.close();
  await connection.close(); 
  console.log("Connection closed")
}

async function sendMessageExchange(message) {
  await channel.assertExchange('command-exchange', 'fanout', {durable: true});

  await channel.publish('', 'command-exchange', Buffer.from(JSON.stringify(message)));
  console.log("Message créer le plat envoyé dans l'exchange")
}

// Routes API et traitement des flags.

const data = {flag: flag};
const query = 'INSERT INTO Commande SET ?';

app.get('/createCommand', (req, res) => {

  db.query(query, data, (error, results) => {
    if (error) throw error;
    console.log(results.affectedRows + ' row inserted');
  })
  connectQueueAndSendDatas();

  console.log("Flag envoyé vers BDD")
  res.send('Message créer plat envoyé')
})

app.get('/getCommande', (req, res) => {
  // Lecture du flag
  db.query('SELECT * FROM Commande', (error, results) => {
    if (error) throw error;
    console.log(results)
    res.send(JSON.stringify(results))
  })
})

// Démarrage application
app.listen(8080, () => {
  console.log('Serveur à l\'écoute')
})
